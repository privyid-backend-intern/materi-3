package main

import (
	"github.com/gorilla/mux"
	"go-mux-api/configs"
	"go-mux-api/routes"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter()

	// Run Database
	configs.ConnectDB()

	// Routes
	routes.ShopRoute(router)
	routes.BookRoute(router)

	log.Fatal(http.ListenAndServe(":4000", router))
}
