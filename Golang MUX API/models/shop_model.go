package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Shop struct {
	Id       primitive.ObjectID `json:"id,omitempty"`
	Name     string             `json:"name,omitempty" validate:"required"`
	Location string             `json:"location,omitempty" validate:"required"`
	BookList []Book             `json:"bookList"`
}
