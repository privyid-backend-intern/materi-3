package routes

import (
	"github.com/gorilla/mux"
	"go-mux-api/controllers"
)

func ShopRoute(router *mux.Router) {
	router.HandleFunc("/shop", controllers.CreateShop()).Methods("POST")
	router.HandleFunc("/shop/{shopId}", controllers.GetAShop()).Methods("GET")
	router.HandleFunc("/shop/{shopId}", controllers.EditAShop()).Methods("PUT")
	router.HandleFunc("/shop/{shopId}", controllers.DeleteAShop()).Methods("DELETE")
	router.HandleFunc("/shops", controllers.GetAllShop()).Methods("GET")
}
