package controllers

import (
	"context"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"go-mux-api/configs"
	"go-mux-api/models"
	"go-mux-api/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"time"
)

var shopCollection *mongo.Collection = configs.GetCollection(configs.DB, "shops")
var validate = validator.New()

func CreateShop() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		var shop models.Shop
		defer cancel()

		// Validate request body
		if err := json.NewDecoder(r.Body).Decode(&shop); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			response := responses.ShopResponse{
				Status:  http.StatusBadRequest,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		// Use validator library to validate required fields
		if validationErr := validate.Struct(&shop); validationErr != nil {
			rw.WriteHeader(http.StatusBadRequest)
			response := responses.ShopResponse{
				Status:  http.StatusBadRequest,
				Message: "error",
				Data:    map[string]interface{}{"data": validationErr.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		newShop := models.Shop{
			Id:       primitive.NewObjectID(),
			Name:     shop.Name,
			Location: shop.Location,
		}

		_, err := shopCollection.InsertOne(ctx, newShop)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			response := responses.ShopResponse{
				Status:  http.StatusInternalServerError,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
		}

		// Success
		rw.WriteHeader(http.StatusCreated)
		response := responses.ShopResponse{
			Status:  http.StatusCreated,
			Message: "success",
			Data:    map[string]interface{}{"id": newShop.Id},
		}
		json.NewEncoder(rw).Encode(response)
	}
}

func GetAShop() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		params := mux.Vars(r)
		shopId := params["shopId"]
		var shop models.Shop
		defer cancel()

		objId, _ := primitive.ObjectIDFromHex(shopId)

		err := shopCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&shop)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			response := responses.ShopResponse{
				Status:  http.StatusInternalServerError,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		rw.WriteHeader(http.StatusOK)
		response := responses.ShopResponse{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": shop}}
		json.NewEncoder(rw).Encode(response)
	}
}

func EditAShop() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		params := mux.Vars(r)
		shopId := params["shopId"]
		var shop models.Shop
		defer cancel()

		objId, _ := primitive.ObjectIDFromHex(shopId)

		//validate the request body
		if err := json.NewDecoder(r.Body).Decode(&shop); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			response := responses.ShopResponse{
				Status:  http.StatusBadRequest,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		//use the validator library to validate required fields
		if validationErr := validate.Struct(&shop); validationErr != nil {
			rw.WriteHeader(http.StatusBadRequest)
			response := responses.ShopResponse{
				Status:  http.StatusBadRequest,
				Message: "error",
				Data:    map[string]interface{}{"data": validationErr.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		update := bson.M{"name": shop.Name, "location": shop.Location}

		result, err := shopCollection.UpdateOne(ctx, bson.M{"id": objId}, bson.M{"$set": update})
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			response := responses.ShopResponse{
				Status:  http.StatusInternalServerError,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		//get updated shop details
		var updatedShop models.Shop
		if result.MatchedCount == 1 {
			err := shopCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&updatedShop)

			if err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				response := responses.ShopResponse{
					Status:  http.StatusInternalServerError,
					Message: "error",
					Data:    map[string]interface{}{"data": err.Error()},
				}
				json.NewEncoder(rw).Encode(response)
				return
			}
		}

		rw.WriteHeader(http.StatusOK)
		response := responses.ShopResponse{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": updatedShop},
		}
		json.NewEncoder(rw).Encode(response)
	}
}

func DeleteAShop() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		params := mux.Vars(r)
		shopId := params["shopId"]
		defer cancel()
		objId, _ := primitive.ObjectIDFromHex(shopId)

		result, err := shopCollection.DeleteOne(ctx, bson.M{"id": objId})

		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			response := responses.ShopResponse{
				Status:  http.StatusInternalServerError,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		if result.DeletedCount < 1 {
			rw.WriteHeader(http.StatusNotFound)
			response := responses.ShopResponse{
				Status:  http.StatusNotFound,
				Message: "error",
				Data:    map[string]interface{}{"data": "Shop with specified ID not found!"},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		rw.WriteHeader(http.StatusOK)
		response := responses.ShopResponse{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": "Shop has been successfully deleted!"},
		}
		json.NewEncoder(rw).Encode(response)
	}
}

func GetAllShop() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		var shops []models.Shop
		defer cancel()

		results, err := shopCollection.Find(ctx, bson.M{})

		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			response := responses.ShopResponse{
				Status:  http.StatusInternalServerError,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		// Reading data from DB
		defer results.Close(ctx)
		for results.Next(ctx) {
			var singleShop models.Shop
			if err = results.Decode(&singleShop); err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				response := responses.ShopResponse{
					Status:  http.StatusInternalServerError,
					Message: "error",
					Data:    map[string]interface{}{"data": err.Error()},
				}
				json.NewEncoder(rw).Encode(response)
			}

			shops = append(shops, singleShop)
		}

		rw.WriteHeader(http.StatusOK)
		response := responses.ShopResponse{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": shops},
		}
		json.NewEncoder(rw).Encode(response)
	}
}
